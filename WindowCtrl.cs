using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Scene_Stager
{
	/// <summary>
	/// CLass that controls the window events
	/// </summary>
	/// 
	public class WindowCtrl
	{
		public Scene scene; 
		/*****************************************************************/
		/*The following is the constructor of WindowCtrl				 */
		/*****************************************************************/		
		public WindowCtrl()
		{

		}

		/***************************************/
		/*Creates a new empty scene window   */
		/***************************************/
		public void createNewEmptyScene()
		{
            //TODO ask user to adjust dimensions

            scene = new Scene(0,0);
            scene.MdiParent = Stager.ActiveForm;
            scene.Activate();
            scene.draw();
            scene.Show();			
			Stager.setStatusBar("New file opened");
		}

		/*****************************************************************/
		/*The following 2 functions are used to rearrange windows' layout*/
		/*****************************************************************/
		public void tileHorizontal()
	    {
			Stager.ActiveForm.LayoutMdi( MdiLayout.TileHorizontal );
		}

		public void tileVertical()

		{
			Stager.ActiveForm.LayoutMdi( MdiLayout.TileVertical );
		}

		//*******************************************/
		/*Open the current scene in a new window  */
		/********************************************/
		public void openInANewWindow()
		{
			
            Scene cur = ((Scene)(Stager.activeScene.ActiveMdiChild));
            if (cur != null)
            {
                Scene scene = new Scene(cur.width,cur.height);
                scene.MdiParent = Stager.ActiveForm;

                scene.sceneItemsList = cur.sceneItemsList;
                scene.backgroundImagePath = cur.backgroundImagePath;
                scene.height = cur.height;
                scene.width = cur.width;

                scene.draw();
                scene.Show();
            }
            else
            {
                MessageBox.Show("Please create a scene first.", "Information");
            }
          
            
		}

		
		
	}
}
