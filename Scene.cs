using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Scene_Stager
{
	/// <summary>
	/// Summary description for Scene.
	/// </summary>
	public class Scene : System.Windows.Forms.Form
	{
		public System.Windows.Forms.PictureBox pictureBox1;
		public System.Windows.Forms.PictureBox pictureBox2;
		public System.Windows.Forms.PictureBox pictureBox3;
		public System.Windows.Forms.PictureBox pictureBox4;
		public System.Windows.Forms.PictureBox pictureBox5;
		public Rectangle old;	//rectangular region to be used later
		public Rectangle r;		//rectangular region to be used later
		public int oldXcoord=0;	//X axis position of cursor before drag and drop
		public int oldYcoord=0;	//Y axis position of cursor before drag and drop
		public bool mouseDown;
		//The follows object points to the last clicked PictureBox object
		public System.Windows.Forms.PictureBox activeSceneItem;
		private System.Windows.Forms.Timer timer1;
		private System.ComponentModel.IContainer components;
        //Array that holds all the items, gets frequently updated and is used for saving the scene
        public System.Collections.ArrayList sceneItemsList;
        public String backgroundImagePath = "";
        public float width = 0;
        public float height = 0;
       

        /**********************************************************************/
        /*This is the constructor of a object (scene window)   */
        /**********************************************************************/
        public Scene(float w, float h)
		{
			// Required for Windows Form Designer support
			InitializeComponent();
            width = w;
            height = h;
            
            sceneItemsList = new System.Collections.ArrayList();
            activeSceneItem = null;
            SceneControl.adjustNumSceneWinOpen(1);

        }
        public void updateBackground(String path)
        {
            this.backgroundImagePath = path;

        }
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Scene));
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.pictureBox3 = new System.Windows.Forms.PictureBox();
			this.pictureBox4 = new System.Windows.Forms.PictureBox();
			this.pictureBox5 = new System.Windows.Forms.PictureBox();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.SuspendLayout();
			// 
			// pictureBox1
			// 
			this.pictureBox1.AccessibleDescription = ((string)(resources.GetObject("pictureBox1.AccessibleDescription")));
			this.pictureBox1.AccessibleName = ((string)(resources.GetObject("pictureBox1.AccessibleName")));
			this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("pictureBox1.Anchor")));
			this.pictureBox1.BackColor = System.Drawing.Color.Red;
			this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
			this.pictureBox1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("pictureBox1.Dock")));
			this.pictureBox1.Enabled = ((bool)(resources.GetObject("pictureBox1.Enabled")));
			this.pictureBox1.Font = ((System.Drawing.Font)(resources.GetObject("pictureBox1.Font")));
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("pictureBox1.ImeMode")));
			this.pictureBox1.Location = ((System.Drawing.Point)(resources.GetObject("pictureBox1.Location")));
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("pictureBox1.RightToLeft")));
			this.pictureBox1.Size = ((System.Drawing.Size)(resources.GetObject("pictureBox1.Size")));
			this.pictureBox1.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("pictureBox1.SizeMode")));
			this.pictureBox1.TabIndex = ((int)(resources.GetObject("pictureBox1.TabIndex")));
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Text = resources.GetString("pictureBox1.Text");
			this.pictureBox1.Visible = ((bool)(resources.GetObject("pictureBox1.Visible")));
			this.pictureBox1.LocationChanged += new System.EventHandler(this.pictureBox1_LocationChanged);
			this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseMove);
			// 
			// pictureBox2
			// 
			this.pictureBox2.AccessibleDescription = ((string)(resources.GetObject("pictureBox2.AccessibleDescription")));
			this.pictureBox2.AccessibleName = ((string)(resources.GetObject("pictureBox2.AccessibleName")));
			this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("pictureBox2.Anchor")));
			this.pictureBox2.BackColor = System.Drawing.Color.Yellow;
			this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
			this.pictureBox2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("pictureBox2.Dock")));
			this.pictureBox2.Enabled = ((bool)(resources.GetObject("pictureBox2.Enabled")));
			this.pictureBox2.Font = ((System.Drawing.Font)(resources.GetObject("pictureBox2.Font")));
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("pictureBox2.ImeMode")));
			this.pictureBox2.Location = ((System.Drawing.Point)(resources.GetObject("pictureBox2.Location")));
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("pictureBox2.RightToLeft")));
			this.pictureBox2.Size = ((System.Drawing.Size)(resources.GetObject("pictureBox2.Size")));
			this.pictureBox2.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("pictureBox2.SizeMode")));
			this.pictureBox2.TabIndex = ((int)(resources.GetObject("pictureBox2.TabIndex")));
			this.pictureBox2.TabStop = false;
			this.pictureBox2.Text = resources.GetString("pictureBox2.Text");
			this.pictureBox2.Visible = ((bool)(resources.GetObject("pictureBox2.Visible")));
			this.pictureBox2.LocationChanged += new System.EventHandler(this.pictureBox1_LocationChanged);
			this.pictureBox2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseMove);
			// 
			// pictureBox3
			// 
			this.pictureBox3.AccessibleDescription = ((string)(resources.GetObject("pictureBox3.AccessibleDescription")));
			this.pictureBox3.AccessibleName = ((string)(resources.GetObject("pictureBox3.AccessibleName")));
			this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("pictureBox3.Anchor")));
			this.pictureBox3.BackColor = System.Drawing.Color.Blue;
			this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
			this.pictureBox3.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("pictureBox3.Dock")));
			this.pictureBox3.Enabled = ((bool)(resources.GetObject("pictureBox3.Enabled")));
			this.pictureBox3.Font = ((System.Drawing.Font)(resources.GetObject("pictureBox3.Font")));
			this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
			this.pictureBox3.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("pictureBox3.ImeMode")));
			this.pictureBox3.Location = ((System.Drawing.Point)(resources.GetObject("pictureBox3.Location")));
			this.pictureBox3.Name = "pictureBox3";
			this.pictureBox3.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("pictureBox3.RightToLeft")));
			this.pictureBox3.Size = ((System.Drawing.Size)(resources.GetObject("pictureBox3.Size")));
			this.pictureBox3.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("pictureBox3.SizeMode")));
			this.pictureBox3.TabIndex = ((int)(resources.GetObject("pictureBox3.TabIndex")));
			this.pictureBox3.TabStop = false;
			this.pictureBox3.Text = resources.GetString("pictureBox3.Text");
			this.pictureBox3.Visible = ((bool)(resources.GetObject("pictureBox3.Visible")));
			this.pictureBox3.LocationChanged += new System.EventHandler(this.pictureBox1_LocationChanged);
			this.pictureBox3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseMove);
			// 
			// pictureBox4
			// 
			this.pictureBox4.AccessibleDescription = ((string)(resources.GetObject("pictureBox4.AccessibleDescription")));
			this.pictureBox4.AccessibleName = ((string)(resources.GetObject("pictureBox4.AccessibleName")));
			this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("pictureBox4.Anchor")));
			this.pictureBox4.BackColor = System.Drawing.Color.Green;
			this.pictureBox4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.BackgroundImage")));
			this.pictureBox4.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("pictureBox4.Dock")));
			this.pictureBox4.Enabled = ((bool)(resources.GetObject("pictureBox4.Enabled")));
			this.pictureBox4.Font = ((System.Drawing.Font)(resources.GetObject("pictureBox4.Font")));
			this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
			this.pictureBox4.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("pictureBox4.ImeMode")));
			this.pictureBox4.Location = ((System.Drawing.Point)(resources.GetObject("pictureBox4.Location")));
			this.pictureBox4.Name = "pictureBox4";
			this.pictureBox4.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("pictureBox4.RightToLeft")));
			this.pictureBox4.Size = ((System.Drawing.Size)(resources.GetObject("pictureBox4.Size")));
			this.pictureBox4.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("pictureBox4.SizeMode")));
			this.pictureBox4.TabIndex = ((int)(resources.GetObject("pictureBox4.TabIndex")));
			this.pictureBox4.TabStop = false;
			this.pictureBox4.Text = resources.GetString("pictureBox4.Text");
			this.pictureBox4.Visible = ((bool)(resources.GetObject("pictureBox4.Visible")));
			this.pictureBox4.LocationChanged += new System.EventHandler(this.pictureBox1_LocationChanged);
			this.pictureBox4.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseMove);
			// 
			// pictureBox5
			// 
			this.pictureBox5.AccessibleDescription = ((string)(resources.GetObject("pictureBox5.AccessibleDescription")));
			this.pictureBox5.AccessibleName = ((string)(resources.GetObject("pictureBox5.AccessibleName")));
			this.pictureBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("pictureBox5.Anchor")));
			this.pictureBox5.BackColor = System.Drawing.Color.Orange;
			this.pictureBox5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox5.BackgroundImage")));
			this.pictureBox5.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("pictureBox5.Dock")));
			this.pictureBox5.Enabled = ((bool)(resources.GetObject("pictureBox5.Enabled")));
			this.pictureBox5.Font = ((System.Drawing.Font)(resources.GetObject("pictureBox5.Font")));
			this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
			this.pictureBox5.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("pictureBox5.ImeMode")));
			this.pictureBox5.Location = ((System.Drawing.Point)(resources.GetObject("pictureBox5.Location")));
			this.pictureBox5.Name = "pictureBox5";
			this.pictureBox5.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("pictureBox5.RightToLeft")));
			this.pictureBox5.Size = ((System.Drawing.Size)(resources.GetObject("pictureBox5.Size")));
			this.pictureBox5.SizeMode = ((System.Windows.Forms.PictureBoxSizeMode)(resources.GetObject("pictureBox5.SizeMode")));
			this.pictureBox5.TabIndex = ((int)(resources.GetObject("pictureBox5.TabIndex")));
			this.pictureBox5.TabStop = false;
			this.pictureBox5.Text = resources.GetString("pictureBox5.Text");
			this.pictureBox5.Visible = ((bool)(resources.GetObject("pictureBox5.Visible")));
			this.pictureBox5.LocationChanged += new System.EventHandler(this.pictureBox1_LocationChanged);
			this.pictureBox5.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseMove);
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// Scene
			// 
			this.AccessibleDescription = ((string)(resources.GetObject("$this.AccessibleDescription")));
			this.AccessibleName = ((string)(resources.GetObject("$this.AccessibleName")));
			this.AllowDrop = true;
			this.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("$this.Anchor")));
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackColor = System.Drawing.Color.DarkGray;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.pictureBox5,
																		  this.pictureBox3,
																		  this.pictureBox4,
																		  this.pictureBox2,
																		  this.pictureBox1});
			this.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("$this.Dock")));
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "Scene";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.Visible = ((bool)(resources.GetObject("$this.Visible")));
			this.DragOver += new System.Windows.Forms.DragEventHandler(this.Scene_DragOver);
			this.DragDrop += new System.Windows.Forms.DragEventHandler(this.Scene_DragDrop);
			this.Closed += new System.EventHandler(this.Scene_Closed);
			this.ResumeLayout(false);

		}
		#endregion

		/**********************************************************************/
		/*This function specifies what objects can be dragged over the scene*/
		/**********************************************************************/
		private void Scene_DragOver(object sender, System.Windows.Forms.DragEventArgs e)
		{
			if (e.Data.GetDataPresent("Image"))
				e.Effect = DragDropEffects.All;
			else 
				e.Effect = DragDropEffects.None;
		}

		/**********************************************************************/
		/*This function...*/
		/**********************************************************************/
		private void Scene_Closed(object sender, System.EventArgs e)
		{
			

			if (SceneControl.getNumScenesOpen() > 0)
			{
                SceneControl.adjustNumSceneWinOpen(-1);
            }
		}

		/*****************************************************************/
		/*This function handles click event on a scene element		 */
		/*****************************************************************/
		private void pictureBox_Click(object sender, System.EventArgs e)
		{
			this.activeSceneItem = (PictureBox)sender;
		}

		/*****************************************************************/
		/*This function handles moving the drag and drop of an element	 */
		/*****************************************************************/
		private void pictureBox_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button.Equals(System.Windows.Forms.MouseButtons.Left))
			{
				this.activeSceneItem = (PictureBox)sender;
				PictureBox temp = this.activeSceneItem;
				
				old = new Rectangle(temp.Left, temp.Top, temp.Width, temp.Height);
				old = this.RectangleToClient(old);
				
				oldXcoord = e.X;
				oldYcoord = e.Y;

				DataObject data = new DataObject("Image", temp);
				temp.DoDragDrop(data, DragDropEffects.All);
				//checking if there is overlap
				Stager.activeScene.objCtrl.detectOverlap(oldXcoord,oldYcoord);
			}
		}

		/*****************************************************************/
		/*This function handles drag and drop operations over the scene*/
		/*****************************************************************/
		private void Scene_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{
			if (e.Data.GetDataPresent("Image"))
			{
				PictureBox o = (PictureBox) e.Data.GetData("Image");
				PictureBox b = o;
                Scene currentScene = ((Scene)(Stager.activeScene.ActiveMdiChild));

                int displacementX = e.X - oldXcoord;
				int displacementY = e.Y - oldYcoord;

				int temp_posX = currentScene.activeSceneItem.Left;
				int temp_posY = currentScene.activeSceneItem.Top;
							
				r = new Rectangle(displacementX, displacementY, o.Width, o.Height);
				r = this.RectangleToClient(r);

				b.Location = r.Location;
			}//end of top if
		}//end of function

        /******************************************************************/
        /*This function draws elements found in the sceneItemsList */
		/******************************************************************/
        public void draw()
		{
            int totalItems = this.sceneItemsList.Count;
            this.Text = "Scene - " + "w: " + width.ToString() + " h: " + height.ToString();

            if (this.backgroundImagePath.Length > 0)
            {
                this.BackgroundImageLayout = ImageLayout.Stretch;
                this.BackgroundImage = System.Drawing.Image.FromFile(this.backgroundImagePath);
            }

            if ( 0 == totalItems )
				drawNone();//draw nothing if no entries in arraylist
            else
            {
                //TODO draw items
            }

		}
		private void drawNone()
		{
			Stager.setStatusBar("Nothing was drawn!");
		}

		/*This function is used by all functions above and is used to draw*/
		/*the item that was passed as a parameter in the correct place on */
		/*the scene													  */
		private void commonActions(SceneItem temp)
		{
			Stager.activeScene.objCtrl.drawANY(temp);
			if (temp.getHorizFlipState()==-1)
				Stager.activeScene.objCtrl.flipHorizontal();
			if (temp.getVertFlipState()==-1)
				Stager.activeScene.objCtrl.flipVertical();

			if (temp.getRotateState()==2)
				Stager.activeScene.objCtrl.rotateLeft();
			else if (temp.getRotateState()==3)
			{
				Stager.activeScene.objCtrl.rotateLeft();
				Stager.activeScene.objCtrl.rotateLeft();
			}
			else if (temp.getRotateState()==4)
				Stager.activeScene.objCtrl.rotateRight();

			Stager.activeScene.objCtrl.changePosition(temp.getPositionX(),temp.getPositionY());
		}

		/*****************************************************************/
		/*This function is called at every tick of the timer and it is   */
		/*used to updates all views of the current scene				 */
		/*****************************************************************/
		private void timer1_Tick(object sender, System.EventArgs e)
		{
			
		}

		/*****************************************************************/
		/*This function sets the moving object as an active device		 */
		/*****************************************************************/
		private void pictureBox1_LocationChanged(object sender, System.EventArgs e)
		{
			this.activeSceneItem = (PictureBox) sender;
		}
	}
}
