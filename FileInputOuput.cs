using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Scene_Stager
{
	/// <summary>
	///  Class that control the input and output operations of files
	/// </summary>
	/// 	
	public class FileInputOuput
	{	
		/*****************************************************************/
		/*The following function is the constructor of FileInputOuput    */
		/*****************************************************************/
		public FileInputOuput()
		{

		}

        /**************************************************/
        /* This method reads an image from a file	      */
        /**************************************************/
        public static void decodeImage(OpenFileDialog openFileDialog1)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK &&
                openFileDialog1.FileName.Length > 0)
            {
                
                if (SceneControl.getNumScenesOpen() > 0)
                {
                    Scene cur = ((Scene)(Stager.activeScene.ActiveMdiChild));
                    cur.backgroundImagePath = openFileDialog1.FileName;
                    cur.BackgroundImageLayout = ImageLayout.Stretch;
                    cur.BackgroundImage = System.Drawing.Image.FromFile(openFileDialog1.FileName);

                    cur.Show();//displaying the scene to the user
                    SceneControl.setElapsedTime();
                    SceneControl.displayElapsedTime("open");
                }
            }
        }

        /**************************************************/
        /* This method reads a Scene from a file	      */
        /**************************************************/
        public static void decodeFromText(OpenFileDialog openFileDialog1)
		{
			if(openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK &&
				openFileDialog1.FileName.Length > 0)
			{   
				//this part of the code is for when there is no scene window
				//that is currently opened
				if ( SceneControl.getNumScenesOpen() == 0)
				{
					Scene scene = new Scene(0,0);
                    scene.MdiParent = Stager.ActiveForm;
					
					//this is the part where loading time is measured
					SceneControl.setInitialTime();
					FileInputOuput.Parse(scene, openFileDialog1);

                    //after loading file into arraylist, draw arraylist elements
                    scene.draw();
                    scene.Show();//displaying the scene to the user
                    SceneControl.setFinalTime();
                    SceneControl.setElapsedTime();
					SceneControl.displayElapsedTime("open");
					

					// Functions to enable or disable menus in the GUI
				
				}
				
				//this part of the code is for when there is already a scene window
				//that is currently opened
				else			
				{
					//this is the part where loading time is measured
					SceneControl.setInitialTime();
                    Scene cur = ((Scene)(Stager.activeScene.ActiveMdiChild));

                    FileInputOuput.Parse(cur,openFileDialog1);

					//after loading file into arraylist, draw arraylist elements
					cur.draw();
					SceneControl.setFinalTime();
					
					// Functions to enable or disable menus in the GUI
				
				}
			}
		}

		/**************************************************/
		/* This method encodes the scene into a file	  */
		/**************************************************/
		public static void encodeToText(SaveFileDialog saveFileDialog1)
		{  	
			if(saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK &&
				saveFileDialog1.FileName.Length > 0) 
			{   
				
                Scene currentScene = ((Scene)(Stager.activeScene.ActiveMdiChild));
                if (currentScene != null)
                {
                    //time measurement starts here
                    SceneControl.setInitialTime();
                    System.IO.StreamWriter outFile = new System.IO.StreamWriter(saveFileDialog1.FileName);
                    if (currentScene.backgroundImagePath.Length > 0)
                    {
                        String toWrite = "SceneBackground;" + currentScene.backgroundImagePath;
                        outFile.WriteLine(toWrite);
                    }

                    {
                        String toWrite = "SceneWidth;" + currentScene.width.ToString();
                        outFile.WriteLine(toWrite);
                    }

                    {
                        String toWrite = "SceneHeight;" + currentScene.height.ToString();
                        outFile.WriteLine(toWrite);
                    }

                    //saving each entry of the arraylist into a line in the file
                    for (int t = 0; t < currentScene.sceneItemsList.Count; t++)
                    {
                        SceneItem tempdevice = (SceneItem)currentScene.sceneItemsList[t];
                        String towrite = "SceneItem;" + tempdevice.ToString();
                        outFile.WriteLine(towrite);
                    }
                    outFile.Close();//closing the file after completion

                    SceneControl.setFinalTime();//time measurement stops here
                    SceneControl.setElapsedTime();
                    SceneControl.displayElapsedTime("save");
                }
			}
		}

		/**************************************************/
		/* This method reads a character stream file in   */
		/* order to generate the arraylist containing all */
		/* scene elements + scene information    		  */
		/**************************************************/
		public static void Parse(Scene curScene, OpenFileDialog openFileDialog1)
		{
			int i = 0;//local throw-away variable
			try 
			{
			// Create an instance of FileStream and StreamReader to read from a file
			System.IO.FileStream file_input = new System.IO.FileStream (openFileDialog1.FileName, 
				System.IO.FileMode.Open,System.IO.FileAccess.Read);

			System.IO.StreamReader sr = new System.IO.StreamReader(file_input);
			String line;
			
			//clearing all contents of the arraylist when loading file
					
			//Constructor needs(name,inputs,outputs,posX,posY,Hflip,Vflip,rotate)
				while ((line = sr.ReadLine ()) != null)
				{
                char[] delimiterChars = { ';' };
                   
                
				String [] tokens = line.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);

                i = 0;
                String type = (tokens[i]);//read type
                i++;

                if (type.CompareTo("SceneBackground") == 0)
                {

                    //TODO: change background for scene
                    curScene.updateBackground((tokens[i]));

                }
                else if (type.CompareTo("SceneWidth") == 0)
                {

                    //TODO: change background for scene
                    curScene.width = float.Parse((tokens[i]));

                }
                else if (type.CompareTo("SceneHeight") == 0)
                {

                    //TODO: change background for scene
                    curScene.height = float.Parse((tokens[i]));

                }
                else if (type.CompareTo("SceneItem") == 0)
                {
                    while (tokens[i].Length > 0)
                    {
                        String maker = (tokens[i]);
                        i++;
                        String model = (tokens[i]);
                        i++;
                        String details = (tokens[i]);
                        i++;
                        float cost = float.Parse((tokens[i]));
                        i++;
                        int delay = int.Parse((tokens[i]));
                        i++;
                        String stockStatus = (tokens[i]);
                        i++;
                        String color = (tokens[i]);
                        i++;
                        float width = float.Parse((tokens[i]));
                        i++;
                        float height = float.Parse((tokens[i]));
                        i++;
                        float depth = float.Parse((tokens[i]));
                        i++;
                        String picture = (tokens[i]);
                        i++;
                        int posX = int.Parse(tokens[i]);//read position in X
                        i++;
                        int posY = int.Parse(tokens[i]);//read position in Y
                        i++;
                        int hFlipState = int.Parse(tokens[i]);//read horizontal flip state
                        i++;
                        int vFlipState = int.Parse(tokens[i]);//read vertical flip state
                        i++;
                        int rotState = int.Parse(tokens[i]);//read rotation state

                        //create and add device to arraylist
                  
                        if (curScene.sceneItemsList.Count < 6)
                        {
                            String[] tempColor = new String[1];
                            String identifier = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff", System.Globalization.CultureInfo.InvariantCulture);
                            tempColor[0] = color;
                            CatalogueItem ci = new CatalogueItem(maker, model, details, cost, delay, stockStatus, tempColor, width, height, depth, picture);
                            SceneItem temp = new SceneItem(ci, identifier, posX, posY, hFlipState, vFlipState, rotState);
                                curScene.sceneItemsList.Add(temp);//adding device to temporary arraylist
                        }
                        }
				}//end of while
				}//end of top while
				sr.Close();
			}//end of try
			catch (Exception e) 
			{
				// Let the user know that something went wrong.
                
				Stager.setStatusBar("Error in reading file!" + e.ToString());
			}
		}//end of function Parse
	}
}
