using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace Scene_Stager
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Stager : System.Windows.Forms.Form
	{
		private System.Windows.Forms.MenuItem fileMenu;
		private System.Windows.Forms.MenuItem newFile;
		private System.Windows.Forms.MenuItem editMenu;
		private System.Windows.Forms.MenuItem open;
		private System.Windows.Forms.MenuItem save;
		private System.Windows.Forms.MenuItem programExit;
		private System.Windows.Forms.MenuItem rotate90Left;
		private System.Windows.Forms.MenuItem rotate90Right;
		private System.Windows.Forms.MenuItem flipHorizontal;
		private System.Windows.Forms.MenuItem flipVertical;
		private System.Windows.Forms.MenuItem deleteSceneItem;
		private System.Windows.Forms.MenuItem tileWindowsMenu;
		private System.Windows.Forms.MenuItem tileHorizontally;
		private System.Windows.Forms.MenuItem tileVertically;
		private System.Windows.Forms.MenuItem helpMenu;
		private System.Windows.Forms.MenuItem guide;
		private System.Windows.Forms.MenuItem aboutBox;
		private System.Windows.Forms.MainMenu mainMenu;
		private System.Windows.Forms.MenuItem separator;
		private System.Windows.Forms.MenuItem separatorEdit;
		private System.Windows.Forms.MenuItem separatorEdit2;
		private System.Windows.Forms.StatusBar statusBar;
		private System.Windows.Forms.MenuItem openNewWindow;
        private IContainer components;
        public static Stager activeScene;
		public WindowCtrl winCtrl;
		public ObjectCtrl objCtrl;
		public SceneControl sceneCtrl;
        private TreeView treeView1;
        private MenuItem menuItem1;
        private MenuItem menuItem2;
        private MenuItem menuItem3;
        private MenuItem menuItem4;
        private MenuItem sceneMenu;
        private MenuItem menuBackground;
        private MenuItem menuItem5;
        private MenuItem menuItem7;
        public const String windowName = "Furniture Stager";


        public Stager()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			winCtrl = new WindowCtrl();
			objCtrl = new ObjectCtrl();
			sceneCtrl = new SceneControl();

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.mainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.fileMenu = new System.Windows.Forms.MenuItem();
            this.newFile = new System.Windows.Forms.MenuItem();
            this.open = new System.Windows.Forms.MenuItem();
            this.save = new System.Windows.Forms.MenuItem();
            this.separator = new System.Windows.Forms.MenuItem();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.programExit = new System.Windows.Forms.MenuItem();
            this.sceneMenu = new System.Windows.Forms.MenuItem();
            this.menuBackground = new System.Windows.Forms.MenuItem();
            this.openNewWindow = new System.Windows.Forms.MenuItem();
            this.tileWindowsMenu = new System.Windows.Forms.MenuItem();
            this.tileHorizontally = new System.Windows.Forms.MenuItem();
            this.tileVertically = new System.Windows.Forms.MenuItem();
            this.editMenu = new System.Windows.Forms.MenuItem();
            this.rotate90Left = new System.Windows.Forms.MenuItem();
            this.rotate90Right = new System.Windows.Forms.MenuItem();
            this.separatorEdit = new System.Windows.Forms.MenuItem();
            this.flipHorizontal = new System.Windows.Forms.MenuItem();
            this.flipVertical = new System.Windows.Forms.MenuItem();
            this.separatorEdit2 = new System.Windows.Forms.MenuItem();
            this.deleteSceneItem = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.helpMenu = new System.Windows.Forms.MenuItem();
            this.guide = new System.Windows.Forms.MenuItem();
            this.aboutBox = new System.Windows.Forms.MenuItem();
            this.statusBar = new System.Windows.Forms.StatusBar();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.fileMenu,
            this.sceneMenu,
            this.editMenu,
            this.menuItem1,
            this.menuItem5,
            this.helpMenu});
            // 
            // fileMenu
            // 
            this.fileMenu.Index = 0;
            this.fileMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.newFile,
            this.open,
            this.save,
            this.separator,
            this.menuItem7,
            this.programExit});
            this.fileMenu.Text = "&File";
            // 
            // newFile
            // 
            this.newFile.Index = 0;
            this.newFile.Shortcut = System.Windows.Forms.Shortcut.CtrlN;
            this.newFile.Text = "New";
            this.newFile.Click += new System.EventHandler(this.newFile_Click);
            // 
            // open
            // 
            this.open.Index = 1;
            this.open.Shortcut = System.Windows.Forms.Shortcut.CtrlO;
            this.open.Text = "Open...";
            this.open.Click += new System.EventHandler(this.open_Click);
            // 
            // save
            // 
            this.save.Index = 2;
            this.save.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.save.Text = "Save...";
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // separator
            // 
            this.separator.Index = 3;
            this.separator.Text = "-";
            // 
            // menuItem7
            // 
            this.menuItem7.Index = 4;
            this.menuItem7.Text = "Refresh Database";
            this.menuItem7.Click += new System.EventHandler(this.refreshDatabase_Click);
            // 
            // programExit
            // 
            this.programExit.Index = 5;
            this.programExit.Shortcut = System.Windows.Forms.Shortcut.AltF4;
            this.programExit.Text = "Exit";
            this.programExit.Click += new System.EventHandler(this.programExit_Click);
            // 
            // sceneMenu
            // 
            this.sceneMenu.Index = 1;
            this.sceneMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuBackground,
            this.openNewWindow});
            this.sceneMenu.Text = "&Scene";
            // 
            // menuBackground
            // 
            this.menuBackground.Index = 0;
            this.menuBackground.Text = "Set Background...";
            this.menuBackground.Click += new System.EventHandler(this.menuBackground_Click);
            // 
            // openNewWindow
            // 
            this.openNewWindow.Index = 1;
            this.openNewWindow.Shortcut = System.Windows.Forms.Shortcut.Ins;
            this.openNewWindow.ShowShortcut = false;
            this.openNewWindow.Text = "Duplicate";
            this.openNewWindow.Click += new System.EventHandler(this.openNewWindow_Click);
            // 
            // tileWindowsMenu
            // 
            this.tileWindowsMenu.Index = 0;
            this.tileWindowsMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.tileHorizontally,
            this.tileVertically});
            this.tileWindowsMenu.Text = "Tile Windows";
            // 
            // tileHorizontally
            // 
            this.tileHorizontally.Index = 0;
            this.tileHorizontally.Text = "Horizontally";
            this.tileHorizontally.Click += new System.EventHandler(this.tileHorizontally_Click);
            // 
            // tileVertically
            // 
            this.tileVertically.Index = 1;
            this.tileVertically.Text = "Vertically";
            this.tileVertically.Click += new System.EventHandler(this.tileVertically_Click);
            // 
            // editMenu
            // 
            this.editMenu.Index = 2;
            this.editMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.rotate90Left,
            this.rotate90Right,
            this.separatorEdit,
            this.flipHorizontal,
            this.flipVertical,
            this.separatorEdit2,
            this.deleteSceneItem});
            this.editMenu.Text = "&Item";
            // 
            // rotate90Left
            // 
            this.rotate90Left.Index = 0;
            this.rotate90Left.Shortcut = System.Windows.Forms.Shortcut.CtrlL;
            this.rotate90Left.Text = "Rotate 90� Left";
            this.rotate90Left.Click += new System.EventHandler(this.rotate90Left_Click);
            // 
            // rotate90Right
            // 
            this.rotate90Right.Index = 1;
            this.rotate90Right.Shortcut = System.Windows.Forms.Shortcut.CtrlR;
            this.rotate90Right.Text = "Rotate 90� Right";
            this.rotate90Right.Click += new System.EventHandler(this.rotate90Right_Click);
            // 
            // separatorEdit
            // 
            this.separatorEdit.Index = 2;
            this.separatorEdit.Text = "-";
            // 
            // flipHorizontal
            // 
            this.flipHorizontal.Index = 3;
            this.flipHorizontal.Shortcut = System.Windows.Forms.Shortcut.CtrlH;
            this.flipHorizontal.Text = "Flip Horizontal";
            this.flipHorizontal.Click += new System.EventHandler(this.flipHorizontal_Click);
            // 
            // flipVertical
            // 
            this.flipVertical.Index = 4;
            this.flipVertical.Shortcut = System.Windows.Forms.Shortcut.CtrlV;
            this.flipVertical.Text = "Flip Vertical";
            this.flipVertical.Click += new System.EventHandler(this.flipVertical_Click);
            // 
            // separatorEdit2
            // 
            this.separatorEdit2.Index = 5;
            this.separatorEdit2.Text = "-";
            // 
            // deleteSceneItem
            // 
            this.deleteSceneItem.Index = 6;
            this.deleteSceneItem.Shortcut = System.Windows.Forms.Shortcut.Del;
            this.deleteSceneItem.Text = "Delete";
            this.deleteSceneItem.Click += new System.EventHandler(this.deleteSceneItem_Click);
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 3;
            this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem2,
            this.menuItem3,
            this.menuItem4});
            this.menuItem1.Text = "&Orders";
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 0;
            this.menuItem2.Text = "Review Current";
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 1;
            this.menuItem3.Text = "Submit Current";
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 2;
            this.menuItem4.Text = "History";
            // 
            // helpMenu
            // 
            this.helpMenu.Index = 5;
            this.helpMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.guide,
            this.aboutBox});
            this.helpMenu.Shortcut = System.Windows.Forms.Shortcut.CtrlA;
            this.helpMenu.Text = "&Help";
            // 
            // guide
            // 
            this.guide.Index = 0;
            this.guide.Shortcut = System.Windows.Forms.Shortcut.F1;
            this.guide.Text = "User Guide";
            this.guide.Click += new System.EventHandler(this.guide_Click);
            // 
            // aboutBox
            // 
            this.aboutBox.Index = 1;
            this.aboutBox.Shortcut = System.Windows.Forms.Shortcut.CtrlA;
            this.aboutBox.Text = "About...";
            this.aboutBox.Click += new System.EventHandler(this.aboutBox_Click);
            // 
            // statusBar
            // 
            this.statusBar.Location = new System.Drawing.Point(0, 289);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(609, 16);
            this.statusBar.TabIndex = 1;
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Right;
            this.treeView1.Location = new System.Drawing.Point(454, 0);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(155, 289);
            this.treeView1.TabIndex = 3;
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 4;
            this.menuItem5.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.tileWindowsMenu});
            this.menuItem5.Text = "&View";
            // 
            // Stager
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(609, 305);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.statusBar);
            this.IsMdiContainer = true;
            this.Menu = this.mainMenu;
            this.MinimumSize = new System.Drawing.Size(350, 129);
            this.Name = windowName;
            this.Text = windowName;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Stager_Closing);
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		/*****************************************************************/
		/*This method is the entry point to the program					 */
		/*****************************************************************/
		static void Main() 
		{
			Application.Run(activeScene = new Stager());

		}

		/*****************************************************************/
		/*This method handles the closing event for a window     */
		/*****************************************************************/
		private void Stager_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{   
			ExitDialog.Show2(e);
		}

		/*****************************************************************/
		/*This method displays the credits window				         */
		/*****************************************************************/
		private void aboutBox_Click(object sender, System.EventArgs e)
		{
			SceneControl.drawCreditWindow();
		}


		/*****************************************************************/
		/*This method displays the User Guide window  			         */
		/*****************************************************************/
		private void guide_Click(object sender, System.EventArgs e)
		{
			SceneControl.drawHelpWindow();
		}

		/*****************************************************************/
		/*This method loads a confirmation box before the program exits  */
		/*****************************************************************/
		private void programExit_Click(object sender, System.EventArgs e)
		{
			ExitDialog.Show();
		}

		/*****************************************************************/
		/*This method loads a file into the program		         */
		/*****************************************************************/
		private void open_Click(object sender, System.EventArgs e)
		{
			SceneControl.fileOpen();
		}

		/*****************************************************************/
		/*This method saves the active scene into a file		         */
		/*****************************************************************/
		private void save_Click(object sender, System.EventArgs e)
		{
			SceneControl.fileSave();
		}

		/*****************************************************************/
		/*This method creates an empty scene							 */
		/*****************************************************************/
		private void newFile_Click(object sender, System.EventArgs e)
		{
			sceneCtrl.windowEvent("newFile");
		}

		/*****************************************************************/
		/*This method tiles all scene windows horizontally			 */
		/*****************************************************************/
		private void tileHorizontally_Click(object sender, System.EventArgs e)
		{
			sceneCtrl.windowEvent("tileHorizontal");
		}

		/*****************************************************************/
		/*This method tiles all scene windows vertically   			 */
		/*****************************************************************/
		private void tileVertically_Click(object sender, System.EventArgs e)
		{
			sceneCtrl.windowEvent("tileVertical");
		}

		/*****************************************************************/
		/*This method open the active scene in  a new window			 */
		/*****************************************************************/
		private void openNewWindow_Click(object sender, System.EventArgs e)
		{
			sceneCtrl.windowEvent("openNewWindow");
		}
        
		/*****************************************************************/
		/*This method activates/deactivates the About menu				 */
		/*****************************************************************/
		public static void setaboutBox(bool enable)
		{
			if (enable == true)
			{
				activeScene.aboutBox.Enabled = true;
			}
			else
			{
				activeScene.aboutBox.Enabled = false;
			}
		}

		/*****************************************************************/
		/*This method activates/deactivates the Help menu				 */
		/*****************************************************************/
		public static void setGuide(bool enable)
		{
			if (enable == true)
			{
				activeScene.guide.Enabled = true;
			}
			else
			{
				activeScene.guide.Enabled = false;
			}
		}

		
		/*****************************************************************/
		/*This method display messages in the status bar    			 */
		/*****************************************************************/	
		public static void setStatusBar(String text)

		{
			activeScene.statusBar.Text = text;
		}

		/*****************************************************************/
		/*This method rotates the active object 90 degrees left			 */
		/*****************************************************************/
		private void rotate90Left_Click(object sender, System.EventArgs e)
		{
			Stager.activeScene.objCtrl.rotateLeft();
		}

		/*****************************************************************/
		/*This method rotates the active object 90 degrees right		 */
		/*****************************************************************/
		private void rotate90Right_Click(object sender, System.EventArgs e)
		{
			Stager.activeScene.objCtrl.rotateRight();
		}

		/*****************************************************************/
		/*This method flips the active object horizontally				 */
		/*****************************************************************/
		private void flipHorizontal_Click(object sender, System.EventArgs e)
		{
			Stager.activeScene.objCtrl.flipHorizontal();
		}

		/*****************************************************************/
		/*This method flips the active object vertically				 */
		/*****************************************************************/
		private void flipVertical_Click(object sender, System.EventArgs e)
		{
			Stager.activeScene.objCtrl.flipVertical();
		}

		/*****************************************************************/
		/*This method removes the active component from the scene		 */
		/*****************************************************************/
		private void deleteSceneItem_Click(object sender, System.EventArgs e)
		{
			Stager.activeScene.objCtrl.deleteSceneItem();
		}

        private void refreshDatabase_Click(object sender, EventArgs e)
        {
            //TODO: populate tree
        }

        private void menuBackground_Click(object sender, EventArgs e)
        {
            SceneControl.setBackground();
        }
    }
}
