using System;

namespace Scene_Stager
{
	/// <summary>
	/// Class that creates and control the catalogue elements
	/// </summary>
	public class CatalogueItem
	{
		String maker;
		String model;
		String details;
		float cost;
		int delay;
		String stockStatus;
		String[] colors;
		float width;
		float height;
		float depth;
		String picture;
		
		/**************************************************************/
		/*The following function is the constructor of CatalogueItem  */
		/**************************************************************/
		public CatalogueItem(		String constructor_maker,
							String constructor_model,
							String constructor_details,
							float constructor_cost,
							int constructor_delay,
							String constructor_stockStatus,
							String[] constructor_colors,
							float constructor_width,
							float constructor_height,
							float constructor_depth,
							String constructor_picture)
		{
            maker = constructor_maker;
            model = constructor_model;
            details = constructor_details;
            cost = constructor_cost;
            delay = constructor_delay;
            stockStatus = constructor_stockStatus;
            colors = constructor_colors;
            width = constructor_width;
            height = constructor_height;
            depth = constructor_depth;
            picture = constructor_picture;
        }
		
	}
}
