using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace Scene_Stager
{
	/// <summary>
	/// Class to show exit confirmation windows
	/// </summary>
	public class ExitDialog
	{	
		/****************************************/
		/*Constructor for the object ExitDialog */
		/****************************************/
		public ExitDialog()
		{
		
		}

		/*******************************************************************************/
		/* Confirmation window to exit the program when the user click on file, exit   */
		/*******************************************************************************/
		public static void Show()
		{
			if (MessageBox.Show ("Are you sure you want to close?", Stager.windowName,
				MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				if (SceneControl.getNumScenesOpen() > 0)
				{
					if (MessageBox.Show ("Do you want to save?", Stager.windowName,
						MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						SceneControl.fileSave();
					}
				}
					Application.Exit();
			}
		}

		/************************************************************************************************/
		/* Confirmation window to exit the program when the user clicks on the "x" bouton on the GUI    */
		/************************************************************************************************/
		public static void Show2(System.ComponentModel.CancelEventArgs e)
		{
			if (MessageBox.Show ("Are you sure you want to close?", Stager.windowName,
				MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (SceneControl.getNumScenesOpen() > 0)
					{
						 if (MessageBox.Show ("Do you want to save?", Stager.windowName,
						 MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
							 {
								 SceneControl.fileSave();
							 }
					 }
					e.Cancel = false;
				}
			else
			{
				e.Cancel = true;
			}		
		}
	}
}
