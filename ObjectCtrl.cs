using System;

namespace Scene_Stager
{
	/// <summary>
	/// Class that controls the object events
	/// </summary>
	public class ObjectCtrl
	{
		//width of an image when it is rotated by 0 or 180 degrees
		private int WIDTH_HORIZONTAL = 100;
		//width of an image when it is rotated by 90 or 270 degrees
		private int WIDTH_VERTICAL = 37;
		//height of an image when it is rotated by 0 or 180 degrees
		private int HEIGHT_HORIZONTAL = 37;
		//height of an image when it is rotated by 90 or 270 degrees
		private int HEIGHT_VERTICAL = 100;

		/*****************************************************************/
		/*The following function is the constructor of ObjectCtrl	     */
		/*****************************************************************/
		public ObjectCtrl()
		{

		}

		/*****************************************************************/
		/*This method rotates the active object 90 degrees to the left   */
		/*****************************************************************/
		public void rotateLeft()
		{
			try
			{
                Scene currentScene = ((Scene)(Stager.activeScene.ActiveMdiChild));
                System.Drawing.Image img = currentScene.activeSceneItem.Image;
				img.RotateFlip(System.Drawing.RotateFlipType.Rotate90FlipNone);
				currentScene.activeSceneItem.Image = img; 
			
				//visual effect
				if (currentScene.activeSceneItem.Height == HEIGHT_VERTICAL)
                    currentScene.activeSceneItem.Height = HEIGHT_HORIZONTAL;
				else
                    currentScene.activeSceneItem.Height = HEIGHT_VERTICAL;
			
				if (currentScene.activeSceneItem.Width == WIDTH_VERTICAL)
                    currentScene.activeSceneItem.Width = WIDTH_HORIZONTAL;
				else
                    currentScene.activeSceneItem.Width = WIDTH_VERTICAL;
				
				//we also need to update the device's values in the arraylist
				int temp_posX = currentScene.activeSceneItem.Left;
				int temp_posY = currentScene.activeSceneItem.Top;

                
                for (int t=0;t< currentScene.sceneItemsList.Count;t++)
					{
					SceneItem temp = (SceneItem)currentScene.sceneItemsList[t];
					if (temp_posX==temp.getPositionX() && temp_posY==temp.getPositionY())
					{
						if (temp.getRotateState() == 1)
							temp.setRotateState(2);
						else if (temp.getRotateState() == 2)
							temp.setRotateState(3);
						else if (temp.getRotateState() == 3)
							temp.setRotateState(4);
						else if (temp.getRotateState() == 4)
							temp.setRotateState(1);
                        currentScene.sceneItemsList[t] = temp;
					}//if correct device is found, modify its rotation variable

					Stager.setStatusBar("SceneItem rotated left 90�");
				}//end of try
			}
			catch (System.NullReferenceException e) 
			{
				Stager.setStatusBar("No device to rotate" + e.ToString());
			}
		}

		/*****************************************************************/
		/*This method rotates the active object 90 degrees to the right  */
		/*****************************************************************/
		public void rotateRight()
		{
			try
			{
            Scene currentScene = ((Scene)(Stager.activeScene.ActiveMdiChild));
            System.Drawing.Image img = currentScene.activeSceneItem.Image;
			img.RotateFlip(System.Drawing.RotateFlipType.Rotate270FlipNone);
            currentScene.activeSceneItem.Image = img; 
			
            //visual effect
			if (currentScene.activeSceneItem.Height == HEIGHT_VERTICAL)
                    currentScene.activeSceneItem.Height = HEIGHT_HORIZONTAL;
			else
                    currentScene.activeSceneItem.Height = HEIGHT_VERTICAL;

			if (currentScene.activeSceneItem.Width == WIDTH_VERTICAL)
                    currentScene.activeSceneItem.Width = WIDTH_HORIZONTAL;
			else
                    currentScene.activeSceneItem.Width = WIDTH_VERTICAL;
			
			//we also need to update the device's values in the arraylist
			int temp_posX = currentScene.activeSceneItem.Left;
			int temp_posY = currentScene.activeSceneItem.Top;
			for(int t=0;t< currentScene.sceneItemsList.Count;t++)
			{
                    SceneItem temp = (SceneItem)currentScene.sceneItemsList[t];
				if (temp_posX==temp.getPositionX() && temp_posY==temp.getPositionY())
				{
					if (temp.getRotateState() == 1)
						temp.setRotateState(4);
					else if (temp.getRotateState() == 2)
						temp.setRotateState(1);
					else if (temp.getRotateState() == 3)
						temp.setRotateState(2);
					else if (temp.getRotateState() == 4)
						temp.setRotateState(3);
                        currentScene.sceneItemsList[t] = temp;
				}//if correct device is found, modify its rotation variable
			}

			Stager.setStatusBar("SceneItem rotated right 90�");
			}//end of try

			catch (System.NullReferenceException e) 
			{
				Stager.setStatusBar("No device to rotate" + e.ToString());
			}
		}

		/***************************************************/
		/*This method flips the active object horizontally */
		/***************************************************/
		public void flipHorizontal()
		{
			try
			{
                Scene currentScene = ((Scene)(Stager.activeScene.ActiveMdiChild));
                System.Drawing.Image img = currentScene.activeSceneItem.Image;
				img.RotateFlip(System.Drawing.RotateFlipType.RotateNoneFlipY);
				currentScene.activeSceneItem.Image = img; 
				
				//we also need to update the device's values in the arraylist
				int temp_posX = currentScene.activeSceneItem.Left;
				int temp_posY = currentScene.activeSceneItem.Top;
				for(int t=0;t< currentScene.sceneItemsList.Count;t++)
				{
					SceneItem temp = (SceneItem)currentScene.sceneItemsList[t];
					if (temp_posX==temp.getPositionX() && temp_posY==temp.getPositionY())
					{
						temp.setHorizFlipState(-1 * temp.getHorizFlipState());
                        currentScene.sceneItemsList[t] = temp;
					}//if correct device is found, modify its vertical flip variable
				}
				Stager.setStatusBar("SceneItem flipped horizontally");
			}

			catch (System.NullReferenceException e) 
			{
			Stager.setStatusBar("No device to flip" + e.ToString());
			}
		}

		/*************************************************/
		/*This method flips the active object vertically */
		/*************************************************/
		public void flipVertical()

		{
			try
			{
                Scene currentScene = ((Scene)(Stager.activeScene.ActiveMdiChild));
                System.Drawing.Image img = currentScene.activeSceneItem.Image;
				img.RotateFlip(System.Drawing.RotateFlipType.RotateNoneFlipX);
                currentScene.activeSceneItem.Image = img; 
				
				//we also need to update the device's values in the arraylist
				int temp_posX = currentScene.activeSceneItem.Left;
				int temp_posY = currentScene.activeSceneItem.Top;
				for(int t=0;t< currentScene.sceneItemsList.Count;t++)
				{
					SceneItem temp = (SceneItem)currentScene.sceneItemsList[t];
					if (temp_posX==temp.getPositionX() && temp_posY==temp.getPositionY())
					{
						temp.setVertFlipState(-1 * temp.getVertFlipState());
                        currentScene.sceneItemsList[t] = temp;
					}//if correct device is found, modify its vertical flip variable
				}
				Stager.setStatusBar("SceneItem flipped vertically");
			}

			catch (System.NullReferenceException e) 
			{
				Stager.setStatusBar("No device to flip" + e.ToString());
			}
			
		}

		/*********************************************************/
		/*This method removes the active object from the scene */
		/*********************************************************/
		public void deleteSceneItem()
		{
			try
			{
                //we need to remove the device values from the arraylist
                Scene currentScene = ((Scene)(Stager.activeScene.ActiveMdiChild));
                int temp_posX = currentScene.activeSceneItem.Left;
				int temp_posY = currentScene.activeSceneItem.Top;
				for(int t=0;t< currentScene.sceneItemsList.Count;t++)
				{
					SceneItem temp = (SceneItem)currentScene.sceneItemsList[t];
					if (temp_posX==temp.getPositionX() && temp_posY==temp.getPositionY())
					{
                        currentScene.sceneItemsList.RemoveAt(t);
					}//if correct device is found, remove it
				}
				//removing item (image representation) from the scene
				for (int a=0;a<Stager.activeScene.MdiChildren.Length;a++)
				{
					
					((Scene)(Stager.activeScene.MdiChildren[a])).activeSceneItem.Image = null;
				}//delete device (image representation) in all windows
				Stager.setStatusBar("SceneItem deleted!");
			}

			catch (System.NullReferenceException e) 
			{
				Stager.setStatusBar("NO DEVICE TO DELETE!" + e.ToString());
			}
		}
		
		/**************************************************************************/
		/*This method changes the position of the active object in the arraylist  */
		/**************************************************************************/
		public void changePosition(int posx,int posy)
		{
			try
			{
                //we need to remove the device values from the arraylist
                Scene currentScene = ((Scene)(Stager.activeScene.ActiveMdiChild));
                int temp_posX = currentScene.activeSceneItem.Left;
				int temp_posY = currentScene.activeSceneItem.Top;
				for(int t=0;t< currentScene.sceneItemsList.Count;t++)
				{
					SceneItem temp = (SceneItem)currentScene.sceneItemsList[t];
					if (temp_posX==temp.getPositionX() && temp_posY==temp.getPositionY())
					{
						temp.setPositionX(posx);
						temp.setPositionY(posy);
                        currentScene.sceneItemsList[t] = temp;
					}//if correct device is found, modify its position variables
				}//end of for
			}//end of try

			catch (System.NullReferenceException e) 
			{
				Stager.setStatusBar("" + e.ToString());
			}
		}

		/**************************************************************************/
		/*This method changes the position of the active object in the arraylist  */
		/**************************************************************************/
		public void detectOverlap(int posx,int posy)
		{
			try
			{
                //we need to retrieve the new location of the active device
                Scene currentScene = ((Scene)(Stager.activeScene.ActiveMdiChild));
                int temp_posX = currentScene.activeSceneItem.Left;
				int temp_posY = currentScene.activeSceneItem.Top;

                //check all entries in the arraylist to find if there is another
                //device in the new position of the active device
                
                for (int t=0;t< currentScene.sceneItemsList.Count;t++)
				{
                    
                    SceneItem temp = (SceneItem) currentScene.sceneItemsList[t];
					
					//if there is already a device in that position, return active device
					//to its original place
					if (temp_posX==temp.getPositionX() && temp_posY==temp.getPositionY())
					{
						temp.setPositionX(posx);
						temp.setPositionY(posy);
                        currentScene.sceneItemsList[t] = temp;
                        currentScene.activeSceneItem.Left = posx;
                        currentScene.activeSceneItem.Top = posy;
					}
				}//end of for
				Stager.setStatusBar("Overlap Occured");
			}
			catch (System.NullReferenceException e) 
			{
				Stager.setStatusBar("" + e.ToString());//if there is no active device
			}
		}
		/************************************************************************/
		/*This method draws an item in the active picture box	*/
		/************************************************************************/
		public void drawANY(SceneItem item)
		
		{	
			try
			{
                /* TODO: Implement
			//allows different file names for different types of devices
			String file_name = portin+"in"+portout+"out.jpg";
			currentScene.activeSceneItem.Image = System.Drawing.Bitmap.FromFile(file_name);
			
			//Create image.
			System.Drawing.Image tmp = ((Scene)(Stager.activeScene.ActiveMdiChild)).activeSceneItem.Image;
			//Create graphics object for alteration.
			System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(tmp);
			//Create string to draw. 
			String wmString = name; 
			//Create font and brush.
			System.Drawing.Font wmFont = new System.Drawing.Font("Arial", 12); 
			System.Drawing.SolidBrush wmBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black); 
			//Create point for upper-left corner of drawing. 
			System.Drawing.PointF wmPoint = new System.Drawing.PointF (29.0F, 10.0F);
			//Draw string to image.
			g.DrawString(wmString, wmFont, wmBrush, wmPoint);
			//Load the new image to picturebox 
			currentScene.activeSceneItem.Image= tmp;        
			//Release graphics object. 
			g.Dispose();

			//Constructor of form (name,inputs,outputs,posX,posY,Hflip,Vflip,rotate)
			SceneItem toAdd = new SceneItem(name,portin,portout,((Scene)(Stager.activeScene.ActiveMdiChild)).activeSceneItem.Left,((Scene)(Stager.activeScene.ActiveMdiChild)).activeSceneItem.Top,1,1,1);
			Stager.activeScene.sceneCtrl.devicesList.Add(toAdd);*/
            }

            catch (System.NullReferenceException e) 
			{
				Stager.setStatusBar("NO DRAWING AREA WAS SELECTED" + e.ToString());
			}
		}
	}
}
