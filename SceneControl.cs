using System;
using System.ComponentModel;
using System.Windows.Forms;


namespace Scene_Stager
{
	/// <summary>
	/// Class that control the overall events and controls
	/// </summary>
	public class SceneControl
	{
	public static int numScenesWinOpen = 0;       // number of windows open
	private static double elapsedTime = 0;			// variable that holds the time to open or save a document 
	private static double initialTimeS = 0;			// time in second before the file the is opened or saved 
	private static double initialTimeMs = 0;
	private static double finalTimeS = 0;			// time in second after the file is opened or saved
	private static double finalTimeMs = 0;

		
		/*****************************************************************/
		/*This method is the constructor of object SceneControl        */
		/*****************************************************************/
		public SceneControl()
		{
            

        }

		/*********************************************************************/
		/*The following functions are used to calculate the save/open delay	 */
		/*********************************************************************/
		public static void setInitialTime()
		{
			initialTimeS = System.DateTime.Now.Second;
			initialTimeMs = System.DateTime.Now.Millisecond;
		}

		public static void setFinalTime()
		{
			finalTimeS = System.DateTime.Now.Second;
			finalTimeMs = System.DateTime.Now.Millisecond;
		}

		public static void setElapsedTime()
		{
			elapsedTime = (finalTimeS-initialTimeS + ((finalTimeMs- initialTimeMs)/1000));
		}

		public static double getElapsedTime()
		{
			return elapsedTime;   
		}

		public static void displayElapsedTime(String fileOperation)
		{
			Stager.setStatusBar("Time to "+ fileOperation + ": " + elapsedTime + " s");
		}

        /*****************************************************************/
        /*The following function loads a properly formatted SceneFile	 */
        /*containing a multitude of components					 */
        /*****************************************************************/
        public static void fileOpen()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "Scene files (*.scn)|*.scn";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            FileInputOuput.decodeFromText(openFileDialog1);
        }

        /*****************************************************************/
        /*The following function loads a properly formatted SceneFile	 */
        /*containing a multitude of components					 */
        /*****************************************************************/
        public static void setBackground()
        {
            if (SceneControl.getNumScenesOpen() > 0)
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();

                openFileDialog1.InitialDirectory = "c:\\";
                openFileDialog1.Filter = "JPG files (*.jpg)|*.jpg";
                openFileDialog1.FilterIndex = 2;
                openFileDialog1.RestoreDirectory = true;

                FileInputOuput.decodeImage(openFileDialog1);
            }
            else
            {
                MessageBox.Show("Please create a scene first.", "Information");
            }
        }

		/*************************************************/
		/*The following function saves a Scene      */
		/*containing a multitude of Scene components   */
		/*************************************************/
		public static void fileSave()
		{
			SaveFileDialog saveFileDialog1 = new SaveFileDialog();
 
			saveFileDialog1.Filter = "Scene files (*.scn)|*.scn"  ;
			saveFileDialog1.FilterIndex = 2 ;
			saveFileDialog1.RestoreDirectory = true ;

			FileInputOuput.encodeToText(saveFileDialog1);

		}		
		
		/*****************************************************************/
		/*The following function calls the appropriate function from	 */
		/*WindowCtrl in order to handle an event that occured			 */
		/*****************************************************************/
		public void windowEvent(String eventFunction)
		{
			if ( eventFunction == "newFile")
			{ 
				Stager.activeScene.winCtrl.createNewEmptyScene();
			}

			if ( eventFunction == "tileHorizontal")
			{
				Stager.activeScene.winCtrl.tileHorizontal();
			}

			if ( eventFunction == "tileVertical")
			{
				Stager.activeScene.winCtrl.tileVertical();
			}

			if ( eventFunction == "openNewWindow")
			{
				Stager.activeScene.winCtrl.openInANewWindow();
			}
		}

		/**********************************************/
		/*Accessor function for */
		/**********************************************/
		public static void adjustNumSceneWinOpen(int plusOrMinus )
		{
            numScenesWinOpen = numScenesWinOpen + plusOrMinus;
            Stager.ActiveForm.Text = Stager.windowName + " - Total Scenes: " + numScenesWinOpen.ToString();

        }

		/*********************************************/
		/*Mutator function for int */
		/*********************************************/
		public static int getNumScenesOpen()
		{
			return numScenesWinOpen;
		}

		/***********************************/
		/*Function to show the help window */
		/***********************************/
		public static void drawHelpWindow()
		{
			HelpWindow.show();
			Stager.setGuide(false);
		}

		/************************************/
		/*Method to show the credit window  */
		/************************************/
		public static void drawCreditWindow()
		{    
			CreditsWindow.show();
			Stager.setaboutBox(false);
		}
	
	}
}
