using System;

namespace Scene_Stager
{
	/// <summary>
	/// Class that creates and control the Scene elements
	/// </summary>
	public class SceneItem
	{
        public CatalogueItem catalogueItem;
        public String identifier;
        public int PositionX;
        public int PositionY;
        public int VertFlipState;
        public int HorizFlipState;
        public int rotState;
		
		/**************************************************************/
		/*The following function is the constructor of object SceneItem  */
		/**************************************************************/
		public SceneItem(	CatalogueItem ci,
                            String inputIdentifier,
                            int posX,
							int posY,
							int Hflip,
							int Vflip,
							int rotate)
		{
			catalogueItem = ci;
            identifier = inputIdentifier;
            PositionX = posX;
			PositionY = posY;
			HorizFlipState = Hflip;
			VertFlipState = Vflip;
			rotState = rotate;
		}

		/********************************************************/
		/* Accessor functions for the properties of the devices */
		/********************************************************/

		public int getPositionX()
		{
			return this.PositionX;
		}
		
		public int getRotateState()
		{
			return this.rotState;
		}

		public int getPositionY()
		{
			return this.PositionY;
		}

		public int getHorizFlipState()
		{
			return this.HorizFlipState;
			
		}

		public int getVertFlipState()
		{
			return this.VertFlipState;
		}

		/********************************************************/
		/* Mutator fonctions for the properties of the devices  */
		/********************************************************/
		public void setPositionX(int loc)
		{
			this.PositionX = loc;
		}

		public void setPositionY(int loc)
		{
			this.PositionY = loc;
		}

		public  void setRotateState(int state)
		{
			this.rotState = state;
		}

		public void setHorizFlipState(int state)
		{
			this.HorizFlipState = state;
			
		}

		public void setVertFlipState(int state)
		{
			this.VertFlipState = state;
		}
		
	}
}
