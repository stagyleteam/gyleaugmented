using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Scene_Stager
{
	/// <summary>
	/// Summary description for HelpWindow.
	/// </summary>
	public class HelpWindow : System.Windows.Forms.Form
	{
		private System.Windows.Forms.RichTextBox richTextBox1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public HelpWindow()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.AutoSize = true;
            this.richTextBox1.Location = new System.Drawing.Point(-4, 12);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(392, 328);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "Features";
            // 
            // HelpWindow
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(400, 353);
            this.Controls.Add(this.richTextBox1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(416, 392);
            this.MinimumSize = new System.Drawing.Size(416, 392);
            this.Name = "HelpWindow";
            this.Text = "HelpWindow";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.HelpWindow_Closing);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/*****************************************************************/
		/*This method displays the help window							 */
		/*****************************************************************/
		public static void show()

		{
			Form help = new HelpWindow();
			help.Show();
			Stager.setStatusBar("Help opened");
		}

		/*****************************************************************/
		/*This method occurs when closing the help window and reactivates*/
		/*the Help menu													 */
		/*****************************************************************/
		private void HelpWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			Stager.setGuide(true);
		}

	}
}
