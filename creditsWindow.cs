using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Scene_Stager
{
	/// <summary>
	/// Summary description for creditsWindow.
	/// </summary>
	public class CreditsWindow : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label title;
		private System.Windows.Forms.Label eddy;
		private System.Windows.Forms.Label label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.title = new System.Windows.Forms.Label();
            this.eddy = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.Location = new System.Drawing.Point(24, 8);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(152, 58);
            this.title.TabIndex = 1;
            this.title.Text = "Programmers";
            this.title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // eddy
            // 
            this.eddy.Location = new System.Drawing.Point(24, 72);
            this.eddy.Name = "eddy";
            this.eddy.Size = new System.Drawing.Size(152, 16);
            this.eddy.TabIndex = 5;
            this.eddy.Text = "Eddy Skaff";
            this.eddy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(40, 152);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 23);
            this.label1.TabIndex = 6;
            this.label1.Text = "Copyright 2017 GYLE";
            // 
            // CreditsWindow
            // 
            this.ClientSize = new System.Drawing.Size(192, 169);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.eddy);
            this.Controls.Add(this.title);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(208, 208);
            this.MinimumSize = new System.Drawing.Size(208, 208);
            this.Name = "CreditsWindow";
            this.Text = "CreditsWindow";
            this.TopMost = true;
            this.Closing += new System.ComponentModel.CancelEventHandler(this.CreditsWindow_Closing);
            this.ResumeLayout(false);

		}
		#endregion

		/*****************************************************************/
		/*The following function is the constructor of CreditsWindow     */
		/*****************************************************************/
		public CreditsWindow()
		{
			// Required for Windows Form Designer support
			InitializeComponent();
		}

		public static void show()

		{
			Form Credits = new CreditsWindow();
			Credits.Show();
			Stager.setStatusBar("About box opened");
		}

		private void CloseWindow_Click(object sender, System.EventArgs e)
		{
			Scene_Stager.CreditsWindow.ActiveForm.Close();
		}

		private void CreditsWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			Stager.setaboutBox(true);
		}
	}
}
